from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.


def home_view(request, *args, **kwargs):  # *args, **kwargs (python specific)
    print(args, kwargs)
    print(request.user)
    # return HttpResponse("<h1>Home Page</h1>")  # str of HTML code
    return render(request, "home.html", {})


def contact_view(request, *args, **kwargs):
    return render(request, "contact.html", {})


def about_view(request, *args, **kwargs):
    my_context = {
        "title": "This is about us",
        "my_number": 123,
        "this_id_trues": True,
        "my_list": [1313, 4231, 312, 'Abc'],
        "my_html": "<h1>Hello, world!</h1>"
    }
    return render(request, "about.html", my_context)


def social_view(request, *args, **kwargs):
    return render(request, "social.html", {})
